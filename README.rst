Docker Images for Sphinx
========================

This provides a small collection of `Docker`_ images for the `Sphinx`_
documentation generator.  It is based on `Alpine Linux`_ and uses the
latest released version of Sphinx with Python 3, currently 8.2.3.

You can pull the most recent pre-built minimal image directly from the
GitLab Container Registry with:

::

   docker pull registry.gitlab.com/paddy-hack/sphinx

Please note that the image uses ``/bin/sh`` rather than ``/bin/bash``.

Specific Versions and Alternative Flavours
------------------------------------------

The image referenced above just gets you the latest, greatest version
du jour.  If you need a specific Sphinx version, you may want to be a
bit more specific.  Please check the `container registry`_ to see what
is currently available.

The images follow a naming convention as illustrated below.  Pick the
image that suits your needs and adjust the version number as needed.

::

   docker pull registry.gitlab.com/paddy-hack/sphinx/alpine
   docker pull registry.gitlab.com/paddy-hack/sphinx/alpine:8.2.3
   docker pull registry.gitlab.com/paddy-hack/sphinx/debian
   docker pull registry.gitlab.com/paddy-hack/sphinx/debian:8.2.3

.. Note:: There used to be Alpine Linux as well as `Debian`_ based
   images for (almost) all Sphinx versions since 1.4.6.  For both, a
   PDF supporting flavour was available as well, although the Alpine
   version only since 3.1.2.  However, storage quota limitations
   `introduced`_ by GitLab.com have forced me to cull a large part of
   the container registry (see #7 for details).


Using the Images
----------------

Assuming your Sphinx documentation lives in the current directory, you
can use something like

::

   docker run --rm -it -v $PWD:/docs -w /docs -u $(id -u):$(id -g) \
       registry.gitlab.com/paddy-hack/sphinx /bin/sh

to start an interactive container.  Your documentation will be volume
mounted on ``/docs``, which is also the working directory inside the
container.  The ``-u`` option will cause all ``sphinx-build`` created
files to be assigned the user and group IDs that you have *outside* of
the container.  This makes life easier if you want to modify or remove
files after you have left the container.

Once in the container, you can use ``sphinx-quickstart`` to get going
or use the ``sphinx-build`` command to maintain your documents.  Of
course, you can also start up a container for a one-off
``sphinx-build`` command.  For example

::

   docker run --rm -t -v $PWD:/docs -w /docs -u $(id -u):$(id -g) \
       registry.gitlab.com/paddy-hack/sphinx \
       sphinx-build -b html . _build/html

would build the HTML version of your documentation.  The ``-t`` option
enables colorized command feedback and is optional.

All images include the ``make`` command.  If you have a ``Makefile``
created by ``sphinx-quickstart`` (or wrote one yourself), you can
simply

::

   docker run --rm -t -v $PWD:/docs -w /docs -u $(id -u):$(id -g) \
       registry.gitlab.com/paddy-hack/sphinx make html


.. _Alpine Linux: https://alpinelinux.org/
.. _Docker: https://www.docker.com/
.. _Sphinx: http://www.sphinx-doc.org/
.. _Debian: https://debian.org/
.. _TeX Live: https://www.tug.org/texlive/
.. _container registry: https://gitlab.com/paddy-hack/sphinx/container_registry
.. _introduced: https://about.gitlab.com/pricing/faq-efficient-free-tier/#storage-limits-on-gitlab-saas-free-tier
