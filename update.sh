#!/bin/sh -eu
# update.sh -- script to bump the upstream version
# Copyright (C) 2017, 2018  Olaf Meeuwissen
#
# License: GPL-v3.0+

release=$(curl --silent \
               https://api.github.com/repos/sphinx-doc/sphinx/tags \
                 | jq --raw-output '.[].name' \
                 | sed 's/^v//; /[^0-9.]/d' \
                 | head -1)

current=$(sed -n "s/^.*_VERSION: \"\([^\"]*\)\".*$/\1/p" .gitlab-ci.yml)

test x$current != x$release || exit 0

if test -z "$(git status --porcelain -- .gitlab-ci.yml README.rst)"; then
    sed -i "s/\(_VERSION: \"\)[^\"]*\(\"\)/\1$release\2/" .gitlab-ci.yml
    sed -i "s/$current/$release/g" README.rst
    git commit -m "Update to Sphinx $release" .gitlab-ci.yml README.rst
else
    echo "Found uncommitted changes in .gitlab-ci.yml and/or README.rst." >&2
    echo "These prevented updating to Sphinx $release." >&2
    exit 1
fi
